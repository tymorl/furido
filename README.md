This is a terrible hack job done on the very nice [Lagrange theme](https://lenpaul.github.io/Lagrange/).
It removed most of the features and did not add much.
If you want to use a nice looking Jekyll theme, I recommend the original (linked above) or something else by that author.

# Mastodon share button

The one thing here that might be useful is the code for the mastodon share button.
It's not great, but it has qualities no other solution I found had, so feel free to use it.
The crucial parts are in `assets/js/mastodon.js` and `_includes/social_sharing.html`.
