"use strict";

const COOKIE_NAME = 'instance-address';

function share(instance, title, link) {
	window.open(`${instance}/share?text=${title}%20$${link}`, "_blank");
}

function mastodonButton(title, link) {
	let instanceAddress = '';
	instanceAddress = getCookie(COOKIE_NAME);
	if (instanceAddress) {
		share(instanceAddress, title, link);
	} else {
		askForAddress();
	}
}

function askForAddress() {
	document.getElementById("mastodon-instance").style.visibility = "visible";
	document.getElementById("mastodon-instance-address").focus();
}

function mastodonForm(title, link) {
	if (addressFromForm()) {
		mastodonButton(title, link);
	}
}

function addressFromForm() {
	let address = document.getElementById("mastodon-instance-address").value;
	if (validateAddress(address)) {
		address = fixAddress(address)
		setCookie(COOKIE_NAME, address, 512);
		return true;
	}
	return false;
}

function validateAddress(address) {
	return address.length > 0;
}

function fixAddress(address) {
	if (address.match("^https?://.*")) {
		return address;
	}
	return "https://" + address;
}

function getCookie(cname) {
	let cookie = {};
	document.cookie.split(';').forEach(function(el) {
		let [k,v] = el.split('=');
		cookie[k.trim()] = v;
	})
	return cookie[cname];
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
