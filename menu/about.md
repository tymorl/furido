---
layout: page
title: Hi!
---

This is a blog you might enjoy reading. I'm an aspiring rationalist, so don't expect too much quality. Enjoy!

### Layout

I took [Lagrange](https://github.com/LeNPaul/Lagrange), removed most of the features and changed almost all the rest.
If you need a good Jekyll theme I'd recommend either Lagrange or something else made by the same guy, he seems smart.
